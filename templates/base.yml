spec:
  inputs:
    cache_dir:
      default: ".cache/apt"
      description: "The path to cache to (relative to CI_PROJECT_DIR)"

---

.debian_backports:
  variables:
    BACKPORTS: "true"

.debian_image:
  image: debian

.debian_cache:
  extends: .debian_image
  variables:
    APT_CACHE_DIR: "$[[ inputs.cache_dir | expand_vars ]]"
  before_script:
    - if [ "$[[ inputs.cache_dir ]]" != "" ]; then
        APT_CACHE_DIR="${CI_PROJECT_DIR}/$[[ inputs.cache_dir ]]";
        echo "Dir::Cache \"${APT_CACHE_DIR}\";" > /etc/apt/apt.conf.d/99cache.conf;
        mkdir -p "${APT_CACHE_DIR}/archives/partial";
      fi
  cache:
    key: "apt-${CI_JOB_NAME_SLUG}"
    paths:
      - "$[[ inputs.cache_dir ]]"

.debian_base:
  extends: .debian_cache
  retry:
    # retry all jobs at most twice when gitlab-ci fails to start the job properly
    # see https://docs.gitlab.com/ee/ci/yaml/#retry
    max: 2
    when:
      - runner_system_failure
  variables:
    # set non-interactive frontend
    DEBIAN_FRONTEND: "noninteractive"
    # name variables for changelog entry (dch)
    DEBFULLNAME: "${GITLAB_USER_NAME}"
    DEBEMAIL: "${GITLAB_USER_EMAIL}"
    # allow `pip install` to install on top of the system installation
    # (this is only CI/CD, what's the worst that could happen)
    PIP_BREAK_SYSTEM_PACKAGES: 1
  before_script:
    # configure backports repo (if requested and not already configured)
    - if ${BACKPORTS:-false} && ! $(grep -qs "backports" /etc/apt/sources.list /etc/apt/sources.list.d/*); then
      . /etc/os-release;
      echo "deb http://deb.debian.org/debian ${VERSION_CODENAME}-backports main" > /etc/apt/sources.list.d/backports.list;
      fi
    # configure debug mode
    - |
      cat > /etc/apt/apt.conf.d/99igwndebug << APT_CONF
      Debug::pkgProblemResolver "true";
      APT_CONF
    # configure caching
    - !reference [.debian_cache, before_script]
    # update package lists
    - apt-get autoclean && apt-get --yes --quiet --quiet --allow-releaseinfo-change-suite update
