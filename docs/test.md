# `debian/test`

Configure jobs to test newly built Debian binary packages (`.deb`).

## Latest release

<!-- markdownlint-disable-next-line line-length -->
[![latest badge](https://git.ligo.org/computing/gitlab/components/debian/-/badges/release.svg?key_text=debian/test&key_width=75)](https://git.ligo.org/explore/catalog/computing/gitlab/components/debian/ "See latest release in the CI/CD catalog")

## Description

This component creates multiple jobs, each with a common prefix, that
take in newly built Debian binary packages (downloaded as artifacts of
other jobs in the pipeline), bundle them into a local Apt repository,
and install them before executing the user-specific test commands.

## Usage

```yaml
include:
  - component: git.ligo.org/computing/gitlab/components/debian/test@<VERSION>
    inputs:
      test_script:
        # sanity check executable installed from Debian package
        - /usr/bin/my-script --help
```

## Inputs

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `stage` | `test` | The stage to add jobs to |
| `debian_versions` { .nowrap } | `[bullseye, bookworm]` { .nowrap } | Debian/Ubuntu versions to test on, see [_Supported versions_](#supported-versions) for details |
| `job_prefix` | `debian_test` | Prefix to use for job name |
| `build_prefix` | `debian_build` | Job name prefix used for build jobs on which to depend |
| `git_strategy` | `fetch` | Value for `GIT_STRATEGY` |
| `test_install` | `""` | Packages to install to support the `test_script` |
| `test_script` | | Script commands to run as part of the debian test job |

## Notes

### Supported versions { data-search-exclude }

See [_Supported versions_](./build.md#supported-versions) in `debian/build`
for details.

### Build image { data-search-exclude }

See [_Build image_](./build.md#build-image) in `debian/build` for details.

Note that for `debian/test` jobs, the default IGWN image is
[`igwn/base`](https://hub.docker.com/r/igwn/base) (not `igwn/builder`).

### Test jobs require matching jobs from `debian/build` { #match }

`debian/test` jobs are automatically configured with `needs` referencing
the job from [`debian/build`](./build.md) with the same `debian_version` value.
It is required that when configuring the `debian/test` you also configure the
`debian/build` component.

See [_Examples_](#examples) for examples.

### Test reports { #reports }

The test jobs configured by `debian/test` include
[`artifacts:report`](https://git.ligo.org/help/ci/yaml/artifacts_reports.html)
as follows:

```yaml
  artifacts:
    reports:
      # coverage report
      coverage_report:
        coverage_format: cobertura
        path: coverage.xml
      # test report
      junit: "**/*junit*.xml"
    paths:
      - "**/.coverage*"
```

This is done to help projects automatically receive test reports and coverage
information on merge request pages.

For projects that do not generate these reports, this will result in the
following output at the end of the build log:

``` { .text .no-copy }
Uploading artifacts...
WARNING: coverage.xml: no matching files. Ensure that the artifact path is relative to the working directory (/builds/duncanmmacleod/component-debian) 
ERROR: No files to upload
```

with the `ERROR` line highlighted in red.
These 'errors' are not fatal and can be safely ignored.

To remove the custom artifacts, override the artifacts setting for the
`debian_test` job configuration

```yaml
debian_test: # (1)!
  artifacts: null
```

1.  The `debian_test` job name should modified to match whatever you set as
    the `job_prefix` input when including the `debian/test` component.

### Test jobs do not automatically install built packages { #no-local-install }

The `debian/test` jobs find all Debian packages (`.deb`) brought in as
artifacts and bundle them into a local Apt repository.
However, starting from __version `2.0.0`__ of this component, the jobs
configured by the `debian/test` component do ___not___ automatically install
_any_ packages; this includes any packages included in the local Apt repository.

To ensure that built packages are installed for testing, include the
appropriate package names in the `test_install` input.

## Examples

!!! example "Build and test a Python application on Debian"

    ```yaml
    include:
      - component: git.ligo.org/computing/gitlab/components/python/sdist@0.1
        inputs:
          stage: source
      - component: git.ligo.org/computing/gitlab/components/debian/build@<VERSION>
        inputs:
          needs: [sdist]
      - component: git.ligo.org/computing/gitlab/components/debian/test@<VERSION>
        inputs:
          test_install: >-
            python3
            python3-mylibrary
            python3-pytest
          test_script:
            - python3 -m pytest --pyargs my_library.tests
    ```
