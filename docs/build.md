# `debian/build`

Configure jobs to build Debian binary package (`.deb`) for this project
from a source distribution or [Debian source package](./source.md).

## Latest release

<!-- markdownlint-disable-next-line line-length -->
[![latest badge](https://git.ligo.org/computing/gitlab/components/debian/-/badges/release.svg?key_text=debian/build&key_width=85)](https://git.ligo.org/explore/catalog/computing/gitlab/components/debian/ "See latest release in the CI/CD catalog")

## Description

This component creates multiple jobs, each with a common prefix, that
take in a pre-existing upstream source distribution (tarball) or Debian
source package, and use
[`dpkg-buildpackage`](https://www.debian.org/doc/manuals/maint-guide/build.en.html)
to create a Debian binary package, one for each chosen Debian distribution
version (selected by the version codename).

After each build, [`lintian`](https://wiki.debian.org/Lintian) is executed
to produce a GitLab
[Code Quality report](https://git.ligo.org/help/ci/testing/code_quality.html)

## Usage

```yaml
include:
  - component: git.ligo.org/computing/gitlab/components/debian/build@<VERSION>
```

## Inputs

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `stage` | `build` | The stage to add jobs to |
| `debian_versions` | `[bullseye, bookworm]` | Debian/Ubuntu versions to build, see [_Supported versions_](#supported-versions) for details |
| `project_dir` | `"."` | Project path that contains the relevant debian/ directory |
| `needs` | `[]` | List of jobs whose artifacts are needed for the Debian build |
| `job_prefix` | `debian_build` | Prefix to use for job name |
| `add_changelog_entry` | `true` | Add a changelog entry when building the Debian source package, see [_Adding changelog entries_](#changelog) for more details |
| `dpkg_buildpackage_options` { .nowrap } | `"-us -uc"` | Options to pass to dpkg-buildpackage |
| `lintian_options` | `--pedantic --display-info` { .nowrap } | Options to pass to lintian, see [_lintianrc_](#lintianrc) for details on using a lintian configuration file. |

## Notes

### Supported versions

The following versions of Debian or Ubuntu are supported, with each using the
listed Docker image for the build.

| Version | Codename | `debian_version` input | Docker image |
| ------- | -------- | ---------------------- | ------------ |
| Debian 11 | Bullseye | `bullseye` | [`igwn/builder`](https://hub.docker.com/r/igwn/builder) |
| Debian 11 backports | Bullseye (backports) | `bullseye-backports` | [`igwn/builder`](https://hub.docker.com/r/igwn/builder) |
| Debian 12 | Bookworm | `bookworm` | [`igwn/builder`](https://hub.docker.com/r/igwn/builder) |
| Debian 13 | Trixie | `trixie` | [`igwn/builder`](https://hub.docker.com/r/igwn/builder) |
| Debian 'oldstable' | | `oldstable` | [`debian`](https://hub.docker.com/_/debian) |
| Debian 'stable' | | `stable` | [`debian`](https://hub.docker.com/_/debian) |
| Debian 'testing' | | `testing` | [`debian`](https://hub.docker.com/_/debian) |
| Debian 'unstable' | | `unstable` | [`debian`](https://hub.docker.com/_/debian) |
| Debian 'experimental' | | `experimental` | [`debian`](https://hub.docker.com/_/debian) |
| Ubuntu 20.04 | Focal Fossa | `focal` | [`ubuntu`](https://hub.docker.com/_/ubuntu) |
| Ubuntu 22.04 | Jammy Jellyfish | `jammy` | [`ubuntu`](https://hub.docker.com/_/ubuntu) |
| Ubuntu 24.04 | Noble Numbat | `noble` | [`ubuntu`](https://hub.docker.com/_/ubuntu) |

For each job, the Debian/Ubuntu version for that job is stored in the
`DEBIAN_VERSION` environment variable.

### Requires an upstream source distribution or Debian source package

Creating a Debian binary package is only supported when starting from an
upstream tarball that contains a `debian/` configuration directory, **OR**
a Debian source package (`.dsc`).

See [_Examples_](#examples) for examples.

### Adding changelog entries { #changelog data-search-exclude }

See [_Adding changelog entries_](./source.md#changelog) in `debian/source`
for details.

## Customisation

### Controlling the `lintian` behaviour { #lintian }

The behaviour of [`lintian`](https://wiki.debian.org/Lintian) can be
controlled in a few different ways (ordered by preference,
most recommended to least):

1.  Create a `{package.,source/}lintian-overrides` file in the `debian/` tree
    for the relevant package.
    See <https://www.debian.org/doc/manuals/maint-guide/dother.en.html#lintian>
    for further details.

2.  Create a `.lintianrc` file in the project root.
    See
    [`lintian(1)`](https://manpages.debian.org/bookworm/lintian/lintian.1.en.html#CONFIGURATION_FILE)
    for details of the lintian configuration file syntax.

    If you wish to choose a path other than `.lintianrc` for the lintian
    configuration file, pass that in `lintian_options`:

    ```yaml
    include:
      - component: git.ligo.org/computing/gitlab/components/debian/build@<VERSION>
        inputs:
          lintian_options: "--cfg mylintianrcfile"
    ```

3.  Pass override arguments via the `lintian_options` input:

    ```yaml
    include:
      - component: git.ligo.org/computing/gitlab/components/debian/build@<VERSION>
        inputs:
          lintian_options: "--suppress-tags python-script-but-no-python-dep"
    ```

## Examples

!!! example "Create Debian source and binary packages separately from a Python 'sdist'"

    ```yaml
    include:
      - component: $CI_SERVER_FQDN/computing/gitlab/components/python/sdist@0.1
        inputs:
          stage: source
      - component: git.ligo.org/computing/gitlab/components/debian/source@<VERSION>
        inputs:
          needs: [sdist]
      - component: git.ligo.org/computing/gitlab/components/debian/build@<VERSION>
        inputs:
          needs: [debian_source]
          debian_versions:
            - bullseye
            - bookworm
    ```

!!! example "Create a Debian binary package directly from a Python 'sdist'"

    ```yaml
    include:
      - component: $CI_SERVER_FQDN/computing/gitlab/components/python/sdist@0.1
        inputs:
          stage: source
      - component: git.ligo.org/computing/gitlab/components/debian/build@<VERSION>
        inputs:
          needs: [sdist]
    ```
