# `debian/source`

Configure a job to build a Debian source package (`.dsc`) for this project.

## Latest release

<!-- markdownlint-disable-next-line line-length -->
[![latest badge](https://git.ligo.org/computing/gitlab/components/debian/-/badges/release.svg?key_text=debian/source&key_width=90)](https://git.ligo.org/explore/catalog/computing/gitlab/components/debian/ "See latest release in the CI/CD catalog")

## Usage

```yaml
include:
  - component: git.ligo.org/computing/gitlab/components/debian/source@<VERSION>
```

## Inputs

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `stage` | `"build"` | The stage to add jobs to |
| `job_name` | `"debian_source"` {: .nowrap } | Name to give this job |
| `image` | [`igwn/builder:bookworm`](https://hub.docker.com/r/igwn/builder) { .nowrap } | Docker image to use when building Debian source package (the built source package is not Debian version-specific) |
| `needs` | | List of jobs whose artifacts are needed for the Debian source build |
| `source_distribution_name` { .nowrap } | `"*.tar.*"` | Name of the source distribution file |
| `add_changelog_entry` | `true` | Add a changelog entry when building the Debian source package, see [_Adding changelog entries_](#changelog) for more details |
| `dpkg_source_options` | `""` | Options to pass to dpkg-source |

## Notes

### Requires an upstream source distribution

Creating a Debian source package is only supported when starting from an
upstream tarball that contains a `debian/` configuration directory.

This must be configured independently of this component and the tarball
provided to the `debian/source` component via the `needs` input.
See [_Examples_](#examples) for an example.

### Adding changelog entries { #changelog }

If `add_changelog_entry: true` is given in `inputs` (the default), the build
will include a call to
[`dch`](https://manpages.debian.org/stable/devscripts/dch.1.en.html) to
add/increment a debian changelog entry to (attempt to) make this build unique.
This happens in one of two ways

1.  If the source distribution includes a
    [`PKG-INFO`](https://peps.python.org/pep-0314/#including-metadata-in-packages)
    file, **and** the `Version` field in that file doesn't match the version
    in the latest Debian changelog entry, a new changelog entry will be added
    using the `--newversion` option to `dch` using the version from the
    `PKG-INFO` file.

    This entry will be tagged with a Debian build number of `9999+debNu1`, where
    `N` is the Debian distribution version number (e.g. `12` for Bookworm).

2.  If the source distribution doesn't include a `PKG-INFO` file, a new
    `--local` changelog entry will be added.

    This entry will be tagged with a Debian build number of `X+debNuY`, where
    `X` is the Debian build number for the latest changelog entry already in
    the changelog, `N` is the Debian distribution version number (e.g. `12`
    for Bookworm), and `Y` is a counter for the number of `+debNu` rebuilds
    (almost always `1`).

## Examples

!!! example "Create a Debian source package from a Python project 'sdist'"

    ```yaml
    include:
      - component: $CI_SERVER_FQDN/computing/gitlab/components/python/sdist@0.1
        inputs:
          stage: source
          job_name: sdist
      - component: git.ligo.org/computing/gitlab/components/debian/source@<VERSION>
        inputs:
          needs: [sdist]
    ```
