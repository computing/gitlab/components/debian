# Debian components

This `computing/gitlab/components/debian` project provides
[CI/CD components](https://git.ligo.org/help/ci/components/index.html)
to help configure a [GitLab CI/CD](https://git.ligo.org/help/ci/index.html) pipeline
for a software project that includes Debian build, test, and quality checking.

## Latest release

<!-- markdownlint-disable-next-line line-length -->
[![latest badge](https://git.ligo.org/computing/gitlab/components/debian/-/badges/release.svg)](https://git.ligo.org/explore/catalog/computing/gitlab/components/debian/ "See latest release in the CI/CD catalog")

## Components

The following components are available:

- [`debian/source`](./source.md "`debian/source` component documentation")
- [`debian/build`](./build.md "`debian/build` component documentation")
- [`debian/test`](./test.md "`debian/test` component documentation")

In addition the following meta-components (combinations) of the above components
are available:

- [`debian/all`](./all.md "`debian/all` component documentation")
