# `debian/all`

Configure an end-to-end Debian build-and-test workflow for this project.

## Latest release

<!-- markdownlint-disable-next-line line-length -->
[![latest badge](https://git.ligo.org/computing/gitlab/components/debian/-/badges/release.svg?key_text=debian/all&key_width=70)](https://git.ligo.org/explore/catalog/computing/gitlab/components/debian/ "See latest release in the CI/CD catalog")

## Description

This component automatically configures a workflow to

- build a Debian [source](./source.md) package
- build one-or-more Debian [binary](./build.md) packages
- install and test the new binary packages

## Usage

```yaml
include:
  - component: git.ligo.org/computing/gitlab/components/debian/all@<VERSION>
```

## Inputs

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `job_prefix` | `"debian"` | Prefix to apply to all job names |
| `debian_versions` | `[bullseye, bookworm]` { .nowrap } | Debian/Ubuntu versions to build/test, see [_Supported versions_](#supported-versions) for details |
| `needs` | | List of jobs whose artifacts are required to create the Debian source package (e.g. the job that created the project source distribution)" |
| `source_distribution_name` | `"*.tar.*"` | Name of the source distribution file |
| `project_dir` | `"."` | Project path that contains the relevant debian/ directory |
| `add_changelog_entry` | `true` | Add a changelog entry when building the Debian source package, see [_Adding changelog entries_](#changelog) for more details |
| `dpkg_source_options` | `""` | Options to pass to dpkg-source |
| `dpkg_buildpackage_options` { .nowrap } | `""` | Options to pass to dpkg-buildpackage |
| `lintian_options` | See below | Options to pass to lintian |
| `test_install` | `""` | Extra packages to install to support the `test_script` |
| `test_script` | | Script commands to run as part of the debian test jobs |

-   The default `lintian_options` value is

    ```text
    --pedantic --display-info --suppress-tags initial-upload-closes-no-bugs
    ```

## Notes

### Supported versions { data-search-exclude }

See [_Supported versions_](./build.md#supported-versions) in `debian/build`
for details.

### Requires an upstream source distribution

Creating Debian packages is only supported when starting from an
upstream tarball that contains a `debian/` configuration directory.

This must be configured independently of this component and the tarball
provided to the `debian/source` component via the `needs` input.
See [_Examples_](#examples) for an example.

### Adding changelog entries { #changelog data-search-exclude }

See [_Adding changelog entries_](./source.md#changelog) in `debian/source`
for details.

## Examples

!!! example "Build and test a Python application on Debian"

    ```yaml
    include:
      - component: git.ligo.org/computing/gitlab/components/python/sdist@0.1
        inputs:
          stage: source
      - component: git.ligo.org/computing/gitlab/components/debian/all@<VERSION>
        inputs:
          needs: [sdist]
          test_install: python3-pytest
          test_script:
            - python3 -m pytest --pyargs my_library.tests
    ```
